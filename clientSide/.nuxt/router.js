import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const _09fa91bb = () => import('../pages/index.vue' /* webpackChunkName: "pages/index" */).then(m => m.default || m)
const _009658a8 = () => import('../pages/worker/index.vue' /* webpackChunkName: "pages/worker/index" */).then(m => m.default || m)
const _61317ed3 = () => import('../pages/attendance/index.vue' /* webpackChunkName: "pages/attendance/index" */).then(m => m.default || m)
const _c83d682c = () => import('../pages/aluminum/index.vue' /* webpackChunkName: "pages/aluminum/index" */).then(m => m.default || m)
const _438145be = () => import('../pages/accessory/index.vue' /* webpackChunkName: "pages/accessory/index" */).then(m => m.default || m)
const _ea3e8456 = () => import('../pages/worker/addWorker.vue' /* webpackChunkName: "pages/worker/addWorker" */).then(m => m.default || m)
const _74741866 = () => import('../pages/attendance/addSheet.vue' /* webpackChunkName: "pages/attendance/addSheet" */).then(m => m.default || m)
const _6344b640 = () => import('../pages/accessory/addItem.vue' /* webpackChunkName: "pages/accessory/addItem" */).then(m => m.default || m)
const _627449e6 = () => import('../pages/accessory/add.vue' /* webpackChunkName: "pages/accessory/add" */).then(m => m.default || m)
const _10410fff = () => import('../pages/aluminum/addSector.vue' /* webpackChunkName: "pages/aluminum/addSector" */).then(m => m.default || m)
const _d7fcfb80 = () => import('../pages/worker/addPosition.vue' /* webpackChunkName: "pages/worker/addPosition" */).then(m => m.default || m)
const _6ced078e = () => import('../pages/aluminum/add.vue' /* webpackChunkName: "pages/aluminum/add" */).then(m => m.default || m)
const _4758f14f = () => import('../pages/worker/edit/_id.vue' /* webpackChunkName: "pages/worker/edit/_id" */).then(m => m.default || m)
const _e016370e = () => import('../pages/accessory/edit/_id.vue' /* webpackChunkName: "pages/accessory/edit/_id" */).then(m => m.default || m)
const _4339c266 = () => import('../pages/aluminum/edit/_id.vue' /* webpackChunkName: "pages/aluminum/edit/_id" */).then(m => m.default || m)
const _189b35e0 = () => import('../pages/worker/_id.vue' /* webpackChunkName: "pages/worker/_id" */).then(m => m.default || m)
const _684540d2 = () => import('../pages/aluminum/_id.vue' /* webpackChunkName: "pages/aluminum/_id" */).then(m => m.default || m)
const _6d819fa6 = () => import('../pages/accessory/_id.vue' /* webpackChunkName: "pages/accessory/_id" */).then(m => m.default || m)



const scrollBehavior = function (to, from, savedPosition) {
      return { x: 0, y: 0 }
    }


export function createRouter () {
  return new Router({
    mode: 'history',
    base: '/',
    linkActiveClass: 'nuxt-link-active',
    linkExactActiveClass: 'nuxt-link-exact-active',
    scrollBehavior,
    routes: [
		{
			path: "/",
			component: _09fa91bb,
			name: "index"
		},
		{
			path: "/worker",
			component: _009658a8,
			name: "worker"
		},
		{
			path: "/attendance",
			component: _61317ed3,
			name: "attendance"
		},
		{
			path: "/aluminum",
			component: _c83d682c,
			name: "aluminum"
		},
		{
			path: "/accessory",
			component: _438145be,
			name: "accessory"
		},
		{
			path: "/worker/addWorker",
			component: _ea3e8456,
			name: "worker-addWorker"
		},
		{
			path: "/attendance/addSheet",
			component: _74741866,
			name: "attendance-addSheet"
		},
		{
			path: "/accessory/addItem",
			component: _6344b640,
			name: "accessory-addItem"
		},
		{
			path: "/accessory/add",
			component: _627449e6,
			name: "accessory-add"
		},
		{
			path: "/aluminum/addSector",
			component: _10410fff,
			name: "aluminum-addSector"
		},
		{
			path: "/worker/addPosition",
			component: _d7fcfb80,
			name: "worker-addPosition"
		},
		{
			path: "/aluminum/add",
			component: _6ced078e,
			name: "aluminum-add"
		},
		{
			path: "/worker/edit/:id?",
			component: _4758f14f,
			name: "worker-edit-id"
		},
		{
			path: "/accessory/edit/:id?",
			component: _e016370e,
			name: "accessory-edit-id"
		},
		{
			path: "/aluminum/edit/:id?",
			component: _4339c266,
			name: "aluminum-edit-id"
		},
		{
			path: "/worker/:id",
			component: _189b35e0,
			name: "worker-id"
		},
		{
			path: "/aluminum/:id",
			component: _684540d2,
			name: "aluminum-id"
		},
		{
			path: "/accessory/:id",
			component: _6d819fa6,
			name: "accessory-id"
		}
    ],
    fallback: false
  })
}
