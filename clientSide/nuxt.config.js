const webpack = require('webpack')
module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Aluminum System',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', content: 'Aluminum System Project' },
      { name: 'msapplication-TileColor', content: '#cfd8dc' },
      { name: 'msapplication-TileImage', content: '/favicons/mstile-144x144.png' },
      { name: 'theme-color', content: '#455a64' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/favicons/favicon-32x32.png' },
      { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/favicons/favicon-16x16.png' },
      { rel: 'apple-touch-icon', sizes: '180x180', href: '/favicons/apple-touch-icon.png' },
      { rel: 'manifest', href: '/favicons/manifest.json' },
      { rel: 'mask-icon', href: '/favicons/safari-pinned-tab.svg', color: '#5bbad5' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Cairo:400,700&amp;subset=arabic' }
    ]
  },
  css: [
    // Css file in the project
    // '~static/css/mainStyle.css',
    // Sass file in the project
    { src: '~assets/sass/mainStyle.scss', lang: 'scss' }, // scss instead of sass
    'vue-multiselect/dist/vue-multiselect.min.css' // vue multiselect css
  ],
  script: [
    // { src: '~assets/js/materialize.min.js' }
  ],
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#b0bec5', height: '3px' },
  /*
  ** Forcing the scroll position to the top
  */
  router: {
    scrollBehavior: function (to, from, savedPosition) {
      return { x: 0, y: 0 }
    }
  },
  build: {
    vendor: ['axios', 'bootstrap', 'jquery', 'materialize-css', 'vue-multiselect'],
    plugins: [
      new webpack.DefinePlugin({
        'process.VERSION': require('./package.json').version
      }),
      // set shortcuts as global for bootstrap
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery'
      })
    ],
    /*
    ** Run ESLINT on save
    */
    extend (config, ctx) {
      if (ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  plugins: [
    '~plugins/jQuery',
    '~plugins/bootstrap.js',
    '~plugins/materialize.js',
    '~plugins/vue-multiselect.js',
    {src: '~plugins/vee-validate.js', ssr: true}
  ]
}
