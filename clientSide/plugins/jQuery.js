/*
 ** Write jQuery for all pages and run it when the page is mounted.
 ** The ';' is not require.
 */
if (process.BROWSER_BUILD) {
  // Import jQuery
  var $ = (window.$ = window.jQuery = require('jquery'))

  // Events and Methods here.. //
  // $('.test').css('color', 'green')
  // $('.subtitle').css('color', 'blue')
  // alert($(window).height())

  $('.phone').on('keydown', function (event) {
    if ((event.which === 38 && event.keyCode === 38) || (event.which === 40 && event.keyCode === 40)) { event.preventDefault() }
  })

  $('.dateTimePicker').on('change', function () {
    $(this).addClass('valid black-text')
  })
}
