// Include materialize JS only

if (process.BROWSER_BUILD) {
  let $ = require('jquery')
  require('materialize-css')

  $('.modal').modal() // modal
  // $('select').material_select() // select
  $('.collapsible').collapsible() // collapsible

  $('.button-collapse').sideNav({
    edge: 'right',
    closeOnClick: true,
    onOpen: function (el) {
      $(el).find('.nuxt-link-exact-active').parents('.collapsible-body').show().siblings('.collapsible-header').addClass('active')
    },
    onClose: function (el) {
      $(el).find('.collapsible-body').hide()
      $(el).find('li').removeClass('active')
      $(el).find('.collapsible-header').removeClass('active')
      $(el).find('.collapsible').collapsible('close', 0)
    }
  }) // side navbar

  $('.timepicker').pickatime({
    default: '7:00AM', // Set default time: 'now', '1:30AM', '16:30'
    fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
    twelvehour: true, // Use AM/PM or 24-hour format
    donetext: 'OK', // text for done-button
    cleartext: 'Clear', // text for clear-button
    canceltext: 'Cancel', // Text for cancel-button
    autoclose: false, // automatic close timepicker
    ampmclickable: true, // make AM PM clickable
    aftershow: function (event) {
      console.log('====================================')
      console.log('val ', event.target.value)
      console.log('====================================')
    } // Function for after opening timepicker
  }) // time picker
}
