import Vue from 'vue'
import VeeValidate, { Validator } from 'vee-validate'
import ar from 'vee-validate/dist/locale/ar'

// Add locale helper.
Validator.localize('ar', ar)

const arAttributes = {
  name: 'الاسم',
  email: 'البريد الاليكتروني',
  phone: 'رقم الهاتف',
  password: 'كلمة السر',
  confirmPassword: 'كلمة السر',
  basicSalary: 'المرتب',
  idNum: 'الرقم القومي',
  address: 'العنوان',
  description: 'الوصف',
  permission: 'الصلاحية',
  startedAt: 'موعد الحضور',
  endedAt: 'موعد الانصراف'
}

const config = {
  errorBagName: 'errors', // change if property conflicts.
  fieldsBagName: 'fields',
  delay: 0.5,
  locale: 'ar',
  dictionary: { ar: {attributes: arAttributes} },
  strict: true,
  classes: false,
  classNames: {
    touched: 'touched', // the control has been blurred
    untouched: 'untouched', // the control hasn't been blurred
    valid: 'valid', // model is valid
    invalid: 'invalid', // model is invalid
    pristine: 'pristine', // control has not been interacted with
    dirty: 'dirty' // control has been interacted with
  },
  events: 'input|blur',
  inject: true,
  validity: false,
  aria: true
}

Vue.use(VeeValidate, config)
