import Axios from '@/services/Api'

export default {
  getAccessories () {
    return Axios().get('accessories')
  },
  addNewAccessory (accessoryDetails) {
    return Axios().post('addAccessory', accessoryDetails)
  }
}
