const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
// const morgan = require('morgan')
const config = require('./config')
const {sequelize} = require('./models')
const app = express()

/** Packages middleware **/
// app.use(morgan('combined'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

app.use(cors())
/** ./Middleware **/

// Routes
require('./routes')(app)

/** Hosting server on a port **/
app.set('port', (config.PORT || 3031))
sequelize.sync()
  .then(() => {
    app.listen(app.get('port'), () => {
      console.log('Server Started on port ' + app.get('port'))
    })
  })
  .catch(err => {
    console.error('Unable to connect to the database: ', err)
  })
