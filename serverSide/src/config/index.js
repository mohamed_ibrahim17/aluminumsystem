module.exports = {
  PORT: process.env.PORT || 3031,
  db: {
    database: process.env.DB_NAME || 'aluminumSys',
    username: process.env.DB_USER || 'aluminumSys',
    password: process.env.DB_PASS || 'aluminumSys',
    options: {
      dialect: process.env.DIALECT || 'sqlite',
      host: process.env.HOST || 'localhost',
      storage: './aluminumSys.sqlite',
      operatorsAliases: false
    }
  }
}
