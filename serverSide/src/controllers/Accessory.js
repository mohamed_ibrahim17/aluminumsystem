const {Accessories} = require('../models') // import Accessories model

module.exports = {
  /**
   * @description A function that add a new Accessory
   *
   * @param {itemName}  @type {String}  // The item name is (Required and Unique) //
   * @param {itemPrice} @type {Integer}  // The item price is (Required) //
   *
   * @returns
   * {
   *  @type {Integer} => status (200 @success / 400 @faild)
   *  @type {Object}  => newaccessory (Contains the new item added)
   *  @type {String}  => message (Confirmation Added Message)
   *  @type {Array}   => error (If error contains the error)
   * }
   */
  async addAccessory (req, res) {
    try {
      const accessories = await Accessories.create(req.body)
      res.status(200).send({
        status: 200,
        newaccessory: accessories.toJSON(),
        message: `The ${req.body.itemName} has been added successfully`
      })
    } catch (error) {
      res.status(400).send({
        status: 400,
        error: error
      })
    }
  },
  /**
   * @description A function that get all Accessories added
   *
   * @returns
   * {
   *  @type {Integer} => status (200 @success / 400 @faild)
   *  @type {Object}  => accessories (Contains all items added)
   *  @type {Array}   => error (If error contains the error)
   * }
   */
  async getAccessories (req, res) {
    try {
      const accessoryItems = await Accessories.findAndCountAll()
      res.status(200).send({
        status: 200,
        accessories: accessoryItems
      })
    } catch (error) {
      res.status(400).send({
        status: 400,
        error: error
      })
    }
  },
  /**
   * @description A function that get details of an Accessory
   *
   * @param {id} @type {Integer}  // The item id (Required) //
   *
   * @returns
   * {
   *  @type {Integer} => status (200 @success / 400 @faild)
   *  @type {Object}  => accessoryDetails (Contains all the item details)
   *  @type {null}    => accessoryDetails (If this item is not excited)
   *  @type {Array}   => error (If error contains the error)
   * }
   */
  async accessoryDetails (req, res) {
    try {
      const accessoryDetails = await Accessories.findById(req.params.id)
      res.status(200).send({
        status: 200,
        accessoryDetails: accessoryDetails
      })
    } catch (error) {
      res.status(400).send({
        status: 400,
        error: error
      })
    }
  },
  /**
   * @description A function that update on an excited Accessory
   *
   * @param {id}         @type {Integer}  // The item id (Required "Selected by default") //
   * @param {itemName}   @type {String}  // The item name is (Required and Unique) //
   * @param {itemPrice}  @type {Integer}  // The item price is (Required) //
   * @param {itemWeight} @type {Integer}  // The item weight is (Optional) //
   * @param {itemNum}    @type {Integer}  // The item count is (Optional) //
   *
   * @returns
   * {
   *  @type {Integer} => status (200 @success / 400 @faild)
   *  @type {Array}   => error (If error contains the error)
   * }
   */
  async updateAccessoryDetails (req, res) {
    try {
      await Accessories.update(
        {itemName: req.body.itemName, itemPrice: req.body.itemPrice, itemWeight: req.body.itemWeight, itemNum: req.body.itemNum},
        {where: {id: req.body.id}}
      )
      res.status(200).send({
        status: 200
      })
    } catch (error) {
      res.status(400).send({
        status: 400,
        error: error
      })
    }
  },
  /**
   * @description A function that delete an Accessory
   *
   * @param {id} @type {Integer}  // The item id (Required "Selected by default") //
   *
   * @returns
   * {
   *  @type {Integer} => status (200 @success / 400 @faild)
   *  @type {Array}   => error (If error contains the error)
   * }
   */
  async deleteAccessory (req, res) {
    try {
      await Accessories.destroy({where: {id: req.body.id}})
      res.status(200).send({
        status: 200
      })
    } catch (error) {
      res.status(400).send({
        status: 400,
        error: error
      })
    }
  }
}
