/**
 * Accessories Table in Database to Add, Show, Update and Delete.
 * @param {*} sequelize // General Sequelize Options Parameter //
 * @param {*} DataTypes // General Sequelize of Datatype Options Parameter //
 * DataTypes Docs -> http://docs.sequelizejs.com/manual/tutorial/models-definition.html#data-types
 * Validation Docs -> http://docs.sequelizejs.com/manual/tutorial/models-definition.html#validations
 */
module.exports = (sequelize, DataTypes) =>
  sequelize.define('Accessories', {
    itemName: {
      type: DataTypes.STRING,
      unique: {
        args: true,
        msg: 'Item name must be unique.'
      },
      allowNull: {
        args: false,
        msg: 'Item name can not be null.'
      },
      validate: {
        notEmpty: {
          args: true,
          msg: 'Item name can not be empty.'
        }
      }
    },
    itemPrice: {
      type: DataTypes.INTEGER,
      allowNull: {
        args: false,
        msg: 'Item price can not be null.'
      },
      validate: {
        isNumeric: {
          args: true,
          msg: 'Item price must be a number.'
        }
      }
    },
    itemWeight: {
      type: DataTypes.INTEGER,
      validate: {
        isNumeric: {
          args: true,
          msg: 'Item weight must be a number.'
        }
      }
    },
    itemNum: {
      type: DataTypes.INTEGER,
      validate: {
        isInt: {
          args: true,
          msg: 'Item number must be an integer.'
        }
      }
    }
  })
