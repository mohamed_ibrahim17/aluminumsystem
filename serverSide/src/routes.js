// _______Accessory Controllers_______ //
const Accessory = require('./controllers/Accessory')
// _______Accessory Controllers_______ //

module.exports = (app) => {
  // _______Accessory Routes_______ //
  app.post('/accessory/add', Accessory.addAccessory)
  app.get('/accessory', Accessory.getAccessories)
  app.get('/accessory/:id', Accessory.accessoryDetails)
  app.put('/accessory/update', Accessory.updateAccessoryDetails)
  app.delete('/accessory/delete', Accessory.deleteAccessory)
  // _______Accessory Routes_______ //
}
